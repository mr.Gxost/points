using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Point : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    [SerializeField]
    private int number;
    
    [SerializeField]
    private AudioClip pointClick;
    
    [SerializeField]
    private GameObject text;
    
    private Dictionary<int, Point> points;
    private bool _isLocked = true;
    private LineRenderer _line;
    private LineRenderer _lineForDrag;
    private Animator _animator;
    private Camera _camera;

    public int Number
    {
        get => number;
        set => number = value;
    }
    
    public GameObject Text
    {
        get => text;
        set => text = value;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        LineRenderer[] lines = FindObjectsOfType<LineRenderer>();
        if (lines.Length > 1)
        {
            LineRenderer line = lines[0].GetComponent<LineRenderer>();
            var lineLastPos = _line.GetPosition(_line.positionCount - 1);
            if (!IsLocked())
            {
                line.SetPosition(0, transform.position);
                RenderLine(transform.position);
                NextPoint();
            }
            else if (transform.position != lineLastPos)
            {
                ErrorPoint(line);
            }
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("click");
        if (!IsLocked())
        {
            RenderLine(transform.position);
            NextPoint();
        }
    }

    private void Awake()
    {
        _line = FindObjectOfType<LineRenderer>();
        _animator = GetComponent<Animator>();
        points = GameManager.Instance.GetPoints();
        _camera = Camera.main;

        if (Number == 1)
        {
            RenderLine(transform.position);
            _animator.SetBool("isNextPoint", true);
            Unlock();
        }

        SetText();
    }

    private LineRenderer CreateLine()
    {
        var line = Instantiate(_line, _line.transform.position, Quaternion.identity);
        line.positionCount = 2;
        line.SetPosition(0, transform.position);
        line.SetPosition(1, transform.position);
        return line;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (_lineForDrag != null)
        {
            Destroy(_lineForDrag.gameObject);
        }
    }
    
    public void OnPointerDown(PointerEventData eventData)
    {
        var lineLastPos = _line.GetPosition(_line.positionCount - 1);
        if (transform.position == lineLastPos)
        {
            if (Number == 1) NextPoint();
            _lineForDrag = CreateLine();
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        var targetPos = _camera.ScreenToWorldPoint(eventData.position);
        targetPos.z = 0;
        _lineForDrag?.SetPosition(1, targetPos);
    }

    public void NextPoint()
    {
        var nextNumber = this.Number + 1;
        Point nextPoint = null;
        GameManager.Instance.PlaySoundByClip(pointClick);
        if (points.TryGetValue(nextNumber, out nextPoint))
        {
            Lock();
            Unlock(nextPoint);
            GetComponent<Animator>().SetBool("isNextPoint", false);
            nextPoint.GetComponent<Animator>().SetBool("isNextPoint", true);
        }
        else
        {
            EventManager.Instance.SetLevelCompleteEvent();
        }
    }

    private void SetText()
    {
        var obj = Instantiate(text, transform.position, Quaternion.identity);
        obj.transform.SetParent(text.transform.parent);
        obj.GetComponent<Text>().text = Number.ToString();
        obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
    }

    private void ErrorPoint(LineRenderer line)
    {
        StartCoroutine(ErrorPointCoroutine(line));
        GameManager.Instance.PlaySoundByName("pointError");
    }

    IEnumerator ErrorPointCoroutine(LineRenderer line)
    {
        var oldStartColor = line.startColor;
        var oldEndColor = line.endColor;
        line.startColor = line.endColor = Color.red;
        yield return new WaitForSeconds(1f);
        if (line != null)
        {
            line.startColor = oldStartColor;
            line.endColor = oldEndColor;
        }
    }

    public bool IsLocked(Point point = null)
    {
        point ??= this;
        return point._isLocked;
    }

    public void Lock(Point point = null)
    {
        point ??= this;
        point._isLocked = true;
    }

    public void Unlock(Point point = null)
    {
        point ??= this;
        point._isLocked = false;
    }

    public void RenderLine(Vector3 targetPos)
    {
        _line.positionCount++;
        _line.SetPosition(_line.positionCount - 1, targetPos);
    }
}