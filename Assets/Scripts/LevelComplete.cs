using UnityEngine;

public class LevelComplete : MonoBehaviour
{
    private void Congratulation()
    {
        GameManager.Instance.PlaySoundByName("levelComplete");
        var points = GameManager.Instance.GetPoints();
        var levelCompleteAnimationPanelAnimator = gameObject.GetComponentInChildren<Animator>(true);
        points[1].transform.parent.gameObject.SetActive(false);
        points[1].Text.transform.parent.gameObject.SetActive(false);
        var lineRenderers = FindObjectsOfType<LineRenderer>();
        foreach (var lineRenderer in lineRenderers)
        {
            lineRenderer.enabled = false;
        }
        levelCompleteAnimationPanelAnimator.gameObject.SetActive(true);
        Invoke("ShowMenu", 3f);
    }

    private void ShowMenu()
    {
        var menu = GameManager.Instance.GetMenuComponent();
        var levelCompleteAnimationPanelAnimator = gameObject.GetComponentInChildren<Animator>(true);
        levelCompleteAnimationPanelAnimator.gameObject.SetActive(false);
        menu.MenuPanel.SetActive(true);
    }
    private void OnEnable()
    {
        EventManager.OnLevelComplete += Congratulation;
    }

    private void OnDisable()
    {
        EventManager.OnLevelComplete -= Congratulation;
    }
}
