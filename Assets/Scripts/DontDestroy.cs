using UnityEngine;

public class DontDestroy : MonoBehaviour
{
    private static GameObject obj = null;
    // Start is called before the first frame update
    void Awake()
    {
        if (obj == null)
        {
            obj = gameObject;
            DontDestroyOnLoad(obj);
        }
        else
        {
            Destroy(obj);
        }
    }
}
