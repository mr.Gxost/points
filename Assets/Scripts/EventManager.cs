﻿using System;

public class EventManager
{
    private static EventManager instance;

    public static event Action OnLevelComplete;
    
    public static EventManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new EventManager();
            }

            return instance;
        }
    }
    public void SetLevelCompleteEvent()
    {
        OnLevelComplete?.Invoke();
    }
}