using System;
using UnityEngine;

public class Sounds : MonoBehaviour
{
    [SerializeField]
    private AudioClip[] clips;

    public void PlayOneShotByName(String clipName)
    {
        AudioClip clip = getAudioClipByName(clipName);
        if (clip != null)
        {
            PlayOneShot(clip);
        }
    }

    private AudioClip getAudioClipByName(String clipName)
    {
        foreach (var clip in clips)
        {
            if (clip.name == clipName)
            {
                return clip;
            }
        }

        return null;
    }

    public void PlayOneShot(AudioClip clip)
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.PlayOneShot(clip);
    }
}
