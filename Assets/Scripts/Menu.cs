using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    [SerializeField]
    private GameObject menuPanel;

    public GameObject MenuPanel
    {
        get => menuPanel;
    }
    public void Exit()
    {
        Application.Quit();
    }

    public void SelectLevel(int level)
    {
        GameManager.Instance.Reset();
        var levelName = (level > 0) ? "Level " + level.ToString() : "Menu";
        SceneManager.LoadScene(levelName);
    }

    public void SelectLevelWithDelay(int level)
    {
        StartCoroutine(SelectLevelCoroutine(level));
    }

    IEnumerator SelectLevelCoroutine(int level, float delay = 0.5f)
    {
        yield return new WaitForSeconds(delay);
        SelectLevel(level);
    }

    public void MainMenu()
    {
        SelectLevelWithDelay(0);
    }
}
