using System;
using System.Collections.Generic;
using UnityEngine;

public class GameManager
{
    private static GameManager instance;
    private Menu _menu = null;
    private Dictionary<int, Point> points = new Dictionary<int, Point>();
    private Sounds soundsComponent;

    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new GameManager();
            }

            return instance;
        }
    }

    public Dictionary<int, Point> GetPoints()
    {
        if (points.Count == 0)
        {
            var obj = GameObject.FindObjectsOfType<Point>();

            foreach (Point point in obj)
            {
                points?.Add(point.Number, point);
            }
        }

        return points;
    }

    public void Reset()
    {
        points.Clear();
        _menu = null;
    }

    public Menu GetMenuComponent()
    {
        _menu ??= GameObject.FindObjectOfType<Menu>();
        return _menu;
    }
    
    public void PlaySoundByName(String clipName)
    {
        GetSoundComponent();   
        soundsComponent.PlayOneShotByName(clipName);
    }

    public void PlaySoundByClip(AudioClip clip)
    {
        GetSoundComponent();   
        soundsComponent.PlayOneShot(clip);
    }

    public Sounds GetSoundComponent()
    {
        if (soundsComponent == null)
        {
            soundsComponent = GameObject.FindObjectOfType<Sounds>();
        }

        return soundsComponent;
    }
}